CREATE DATABASE order_demo;
CREATE USER order_demo PASSWORD 'order_demo';
GRANT ALL PRIVILEGES ON DATABASE order_demo TO order_demo;

\c order_demo

CREATE TABLE IF NOT EXISTS orders (
        uuid text,
        username text,
        o0 integer,
        o1 integer,
        o2 integer,
        o3 integer,
        o4 integer,
        o5 integer,
        o6 integer,
        o7 integer,
        o8 integer,
        o9 integer,
        or0 integer,
        or1 integer,
        or2 integer,
        or3 integer,
        or4 integer,
        or5 integer,
        or6 integer,
        or7 integer,
        or8 integer,
        or9 integer,
        PRIMARY KEY (uuid)
);