#!/bin/bash

# create data and blog directories:
mkdir -p ${ZK_DATA_DIR}
mkdir -p ${ZK_DATA_LOG_DIR}

# create myID file:
echo "$ZK_MY_ID" | tee ${ZK_DATA_DIR}/myid

# now build the ZooKeeper configuration file:
ZK_CONFIG=
ZK_CONFIG="$ZK_CONFIG"$'\n'"tickTime=$ZK_TICK_TIME"
ZK_CONFIG="$ZK_CONFIG"$'\n'"dataDir=$ZK_DATA_DIR"
ZK_CONFIG="$ZK_CONFIG"$'\n'"dataLogDir=$ZK_DATA_LOG_DIR"
ZK_CONFIG="$ZK_CONFIG"$'\n'"clientPort=$ZK_CLIENT_PORT"
ZK_CONFIG="$ZK_CONFIG"$'\n'"initLimit=$ZK_INIT_LIMIT"
ZK_CONFIG="$ZK_CONFIG"$'\n'"syncLimit=$ZK_SYNC_LIMIT"
ZK_CONFIG="$ZK_CONFIG"$'\n'"maxClientCnxns=$ZK_MAX_CLIENT_CNXNS"

# now append information on every ZooKeeper node in the ensemble to the ZooKeeper config:
IFS=',' read -r -a ZK_SERVERS_ARRAY <<< "$ZK_SERVERS"

for index in "${!ZK_SERVERS_ARRAY[@]}"
do
    ZK_ID=$(($index+1))
    if [ $ZK_ID = $ZK_MY_ID ]; then
        ZK_IP=0.0.0.0
    else
        ZK_IP=${ZK_SERVERS_ARRAY[index]}
    fi
    ZK_CONFIG="$ZK_CONFIG"$'\n'"server.$ZK_ID=$ZK_IP:2888:3888"
done

# Finally, write config file:
echo "$ZK_CONFIG" | tee $ZK_HOME/conf/zoo.cfg

# start the server:
$ZK_HOME/bin/zkServer.sh start-foreground
