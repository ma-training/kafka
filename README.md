# Train Kafka experience

## Prepare environment

```bash
cd kafka && docker build -t kafka:2.8.1 -f Dockerfile . && cd ..
cd zk && docker build -t zookeeper:3.7.0 -f Dockerfile . && cd ..
docker-compose up -d
docker run -it --rm --network kafka_default kafka:2.8.1 \
  /opt/kafka/bin/kafka-topics.sh --create \
  --topic order-command --partitions 1 --replication-factor 3 \
  --config retention.ms=3600000 \
  --config min.insync.replicas=2 \
  --bootstrap-server kafka1:9092
docker run -it --rm --network kafka_default kafka:2.8.1 \
  /opt/kafka/bin/kafka-topics.sh --create \
  --topic order-event --partitions 1 --replication-factor 3 \
  --config retention.ms=3600000 \
  --config min.insync.replicas=2 \
  --bootstrap-server kafka1:9092
docker run -it --rm --network kafka_default kafka:2.8.1 \
  /opt/kafka/bin/kafka-topics.sh --list \
  --bootstrap-server kafka1:9092
```


## Group monitor

```bash
docker run -it --rm --network kafka_default kafka:2.8.1 \
  /opt/kafka/bin/kafka-consumer-groups.sh --list \
  --bootstrap-server kafka1:9092
docker run -it --rm --network kafka_default kafka:2.8.1 \
  /opt/kafka/bin/kafka-consumer-groups.sh --describe --group order-processor \
  --bootstrap-server kafka1:9092
```

## Topic def

```bash
docker run -it --rm --network kafka_default kafka:2.8.1 \
  /opt/kafka/bin/kafka-topics.sh --describe --topic order-command \
  --bootstrap-server kafka1:9092
docker run -it --rm --network kafka_default kafka:2.8.1 \
  /opt/kafka/bin/kafka-topics.sh --describe --topic order-event \
  --bootstrap-server kafka1:9092
```

## SQL Monitor

```sql
select * from orders;
select count(1) from orders;
-- delete from orders;
select * from orders where
    o0 is null or
    o1 is null or
    o2 is null or
    o3 is null or
    o4 is null or
    o5 is null or
    o6 is null or
    o7 is null or
    o8 is null or
    o9 is null
;
select * from orders where
    or0 > 0 or
    or1 > 0 or
    or2 > 0 or
    or3 > 0 or
    or4 > 0 or
    or5 > 0 or
    or6 > 0 or
    or7 > 0 or
    or8 > 0 or
    or9 > 0
;
```
    
## Run load test

```bash
cd sample-app/cliens-emulator
mvn test gatling:test
```