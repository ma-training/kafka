package pro.omut.training.kafka.clients.emulator;

import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpProtocolBuilder;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.*;

public class ClientSimulation extends Simulation {
    // Protocol Definition
    HttpProtocolBuilder httpProtocol = http
            .baseUrl("http://localhost:8080")
            .acceptHeader("application/json")
            .userAgentHeader("Gatling/Performance Test");

    Iterator<Map<String, Object>> feederUser =
            Stream.generate((Supplier<Map<String, Object>>) ()
                    -> Collections.singletonMap("username", UUID.randomUUID().toString())
            ).iterator();
    Iterator<Map<String, Object>> feederOrderPart =
            Stream.generate((Supplier<Map<String, Object>>) ()
                    -> Collections.singletonMap("order", UUID.randomUUID().toString())
            ).iterator();

    // Scenario
    ScenarioBuilder scn = CoreDsl.scenario("Load Test Creating Customers")
            .feed(feederUser)
            .exec(http("create-client-order")
                    .post("/api/order-command")
                    .header("Content-Type", "application/json")
                    .body(StringBody("{ \"username\": \"${username}\" }"))
                    .check(status().is(200))
                    .check(bodyString().saveAs("uuid")
                    )
            )
            .pause(Duration.ofMillis(100))
            .foreach(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), "order").on(
                    tryMax(5, "counter").on(
                            exec(http("update-client-order")
                                    .patch("/api/order-command/${uuid}")
                                    .header("Content-Type", "application/json")
                                    .body(StringBody("{ \"username\": \"${username}\", \"orderAdd\":${order}, \"orderAddRT\":${counter} }"))
                                    .check(status().is(200))
                            )
                                    .pause(Duration.ofMillis(90)))
            );

    public ClientSimulation() {
        this.setUp(scn.injectOpen(constantUsersPerSec(2).during(Duration.ofMinutes(1))))
                .protocols(httpProtocol);
    }
}
