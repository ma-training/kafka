package pro.omut.training.kafka.app.util;

public interface AppLifecycle {
    void start();
    void stop();
}
