package pro.omut.training.kafka.app.util;

import java.util.concurrent.CountDownLatch;

public class AppBootstrap{
    private final AppLifecycle task;
    private final CountDownLatch latch = new CountDownLatch(1);

    private Thread appThread;

    public AppBootstrap(AppLifecycle task) {
        this.task = task;
    }

    public void start() {
        prepareStop();
        init();
        runApplication();
    }

    private void init() {
        appThread = new Thread("AppBootstrap-work"){
            @Override
            public void run() {
                task.start();
            }
        };
    }

    private void runApplication() {
        try {
            appThread.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }

    private void prepareStop() {
        Runtime.getRuntime().addShutdownHook(new Thread("AppBootstrap-shutdown-hook") {
            @Override
            public void run() {
                task.stop();
                latch.countDown();
            }
        });
    }


}
