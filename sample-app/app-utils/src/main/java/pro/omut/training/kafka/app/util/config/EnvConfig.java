package pro.omut.training.kafka.app.util.config;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EnvConfig {
    public String get(String name) {
        String value = System.getenv(name);
        log.info("Env {} = '{}'", name, value);
        return value;
    }

    public String get(String name, String defaultValue) {
        String value = get(name);
        if (value == null) {
            log.info("Value is null use default {} = '{}'", name, defaultValue);
            return defaultValue;
        } else if (value.trim().isEmpty()) {
            log.info("Value is empty use default {} = '{}'", name, defaultValue);
            return defaultValue;
        }
        return value;
    }
}
