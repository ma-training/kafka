package pro.omut.training.kafka.order.cmd.adapter;

import com.fasterxml.jackson.databind.JsonNode;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Slf4j
public class OrderCMDRequestHandler extends ChannelInboundHandlerAdapter {

    private static final String path = "/order-command";
    private final Producer<String, String> producer;

    public OrderCMDRequestHandler(Producer<String, String> producer) {
        this.producer = producer;
    }


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        if (msg instanceof FullHttpRequest) {
            FullHttpRequest in = (FullHttpRequest) msg;
            log.debug("Server received:{}", in);
            HttpVersion httpVersion = in.protocolVersion();
            log.debug("Protocol {}", httpVersion);
            HttpMethod method = in.method();
            log.debug("Method {}", method);
            HttpHeaders headers = in.headers();
            Set<String> names = headers.names();
            for (String n : names) {
                log.debug("H - {}:{}", n, headers.get(n));
            }
            String uri = in.uri();
            log.debug("URI {}", uri);
            ByteBuf cnt = in.content();
            CharSequence data = cnt.readCharSequence(cnt.capacity(), CharsetUtil.UTF_8);
            log.debug("Content:{}", data);
            String uuid = UUID.randomUUID().toString();
            String xRequestId = uuid;
            if (in.headers().contains("X-Request-ID")) {
                xRequestId = in.headers().get("X-Request-ID");
            }

            if (uri.contains(path)) {
                String[] cmd = uri.substring(uri.indexOf(path)).split("/");
                if (HttpMethod.GET.equals(method)) {
                    if (cmd.length == 3 && !cmd[2].trim().isEmpty()) {
                        log.debug("Not implemented received id: {}", cmd[2].trim());
                        // TODO sync async implementation
                        DefaultFullHttpResponse err = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_IMPLEMENTED);
                        err.headers().set("X-Request-Id", xRequestId);
                        ctx.write(err).addListener(ChannelFutureListener.CLOSE);
                    } else {
                        ByteBuf content = Unpooled.wrappedBuffer(("Invalid URI change to '" + path + "/{uuid}'").getBytes(CharsetUtil.UTF_8));
                        DefaultFullHttpResponse err = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR, content);
                        err.headers().set("X-Request-Id", xRequestId);
                        ctx.write(err).addListener(ChannelFutureListener.CLOSE);
                    }
                } else if (HttpMethod.POST.equals(method)) {
                    if (cmd.length == 2 || (cmd.length == 3 && !cmd[2].trim().isEmpty())) {
                        if (cmd.length == 3 && !cmd[2].trim().isEmpty()) {
                            log.debug("Command uuid: {}", cmd[2].trim());
                            uuid = cmd[2].trim();
                        }
                        sendCommand(ctx, method, data, uuid, xRequestId);
                    } else {
                        ByteBuf content = Unpooled.wrappedBuffer(("Invalid URI change to '" + path + "/[{uuid}]'").getBytes(CharsetUtil.UTF_8));
                        DefaultFullHttpResponse err = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR, content);
                        err.headers().set("X-Request-Id", xRequestId);
                        ctx.write(err).addListener(ChannelFutureListener.CLOSE);
                    }
                } else if (HttpMethod.PATCH.equals(method)) {
                    if (cmd.length == 3 && !cmd[2].trim().isEmpty()) {
                        log.debug("Command uuid: {}", cmd[2].trim());
                        uuid = cmd[2].trim();
                        sendCommand(ctx, method, data, uuid, xRequestId);
                    } else {
                        ByteBuf content = Unpooled.wrappedBuffer(("Invalid URI change to '" + path + "/[{uuid}]'").getBytes(CharsetUtil.UTF_8));
                        DefaultFullHttpResponse err = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR, content);
                        err.headers().set("X-Request-Id", xRequestId);
                        ctx.write(err).addListener(ChannelFutureListener.CLOSE);
                    }
                } else {
                    DefaultFullHttpResponse err = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_IMPLEMENTED);
                    err.headers().set("X-Request-Id", xRequestId);
                    ctx.write(err).addListener(ChannelFutureListener.CLOSE);
                }
            } else {
                DefaultFullHttpResponse err = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND);
                err.headers().set("X-Request-Id", xRequestId);
                ctx.write(err).addListener(ChannelFutureListener.CLOSE);
            }
        }
    }

    private void sendCommand(ChannelHandlerContext ctx, HttpMethod method, CharSequence data, String uuid, String xRequestId) {
        ProducerRecord<String, String> record = new ProducerRecord<>(path.replace("/", ""), uuid, data.toString());
        record.headers().add("method", method.name().getBytes(StandardCharsets.UTF_8));
        Future<RecordMetadata> send = producer.send(record);
        try {
            RecordMetadata recordMetadata = send.get(1000, TimeUnit.MILLISECONDS);
//            boolean done = send.isDone();
            if (send.isDone()) {
                ByteBuf content = Unpooled.wrappedBuffer(uuid.getBytes(CharsetUtil.UTF_8));
                DefaultFullHttpResponse err = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, content);
                err.headers().set("X-Request-Id", xRequestId);
                err.headers().set("Content-Type", "text/plain");
                ctx.write(err).addListener(ChannelFutureListener.CLOSE);
            } else {
                cancelTask(send, xRequestId, ctx);
            }
        } catch (Throwable e) {
            cancelTask(send, xRequestId, ctx);
        }
    }

    private void cancelTask(Future<RecordMetadata> send, String xRequestId, ChannelHandlerContext ctx) {
        send.cancel(true);
        ByteBuf content = Unpooled.wrappedBuffer(("No message sent").getBytes(CharsetUtil.UTF_8));
        DefaultFullHttpResponse err = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR, content);
        err.headers().set("X-Request-Id", xRequestId);
        ctx.write(err).addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx,
                                Throwable cause) {
        log.error("Error handling client ", cause);
        ctx.close();
    }

}