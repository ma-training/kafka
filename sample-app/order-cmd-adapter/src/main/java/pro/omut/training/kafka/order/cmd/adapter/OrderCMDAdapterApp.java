package pro.omut.training.kafka.order.cmd.adapter;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import pro.omut.training.kafka.app.util.AppBootstrap;
import pro.omut.training.kafka.app.util.AppLifecycle;
import pro.omut.training.kafka.app.util.config.EnvConfig;

import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Slf4j
public class OrderCMDAdapterApp implements AppLifecycle {

    public static void main(String[] args) {
        new AppBootstrap(new OrderCMDAdapterApp()).start();
    }

    @Override
    public void start() {
        EnvConfig config = new EnvConfig();
        String kafkaHosts = config.get("KAFKA_HOSTS", "localhost:9092");
        String portString = config.get("PORT", "8080");
        Integer port = 8080;
        try {
            port = Integer.parseInt(portString);
        } catch (Throwable e) {
            log.info("used port default");
        }

        Properties props = new Properties();
        props.put("bootstrap.servers", kafkaHosts);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        Producer<String, String> producer = new KafkaProducer<String, String>(props);

        NioEventLoopGroup loopGroup = new NioEventLoopGroup();
        try {
            Class<NioServerSocketChannel> socketChannelClass = NioServerSocketChannel.class;
            log.info("Prepare to start server");
            log.info("Start test stream");
            final InetSocketAddress inet = new InetSocketAddress(port);
            final ServerBootstrap b = new ServerBootstrap();
            b.group(loopGroup).channel(socketChannelClass).childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    final ChannelPipeline p = ch.pipeline();
                    p.addLast("encoder", new HttpResponseEncoder());
                    p.addLast("decoder", new HttpRequestDecoder(4096, 8192, 8192, false));
                    p.addLast("aggregator", new HttpObjectAggregator(1048576));
                    p.addLast("handler", new OrderCMDRequestHandler(producer));
                }
            });
            log.info("Configure server");
            final Channel ch = b.bind(inet).sync().channel();
            ch.closeFuture().sync();
        } catch (InterruptedException e) {
            log.error("App Interrupted", e);
        } finally {
            producer.close(1000, TimeUnit.MILLISECONDS);
            try {
                loopGroup.shutdownGracefully().sync();
            } catch (InterruptedException e) {
                log.error("App Interrupted at finalize", e);
            }
        }
    }

    @Override
    public void stop() {

    }
}
