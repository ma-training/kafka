package pro.omut.training.kafka.order.processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;

import javax.sql.DataSource;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Slf4j
public class OrderProcessor implements Processor<String, String> {
    private final ObjectMapper om = new ObjectMapper();
    private final DataSource ds;
    private ProcessorContext context;

    public OrderProcessor(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public void init(ProcessorContext context) {
        this.context = context;
    }

    @Override
    public void process(String key, String value) {
        Connection connection = null;
        try {
            connection = ds.getConnection();
            Headers headers = context.headers();
            String method = new String(headers.lastHeader("method").value(), StandardCharsets.UTF_8);
            log.debug("method: {}", method);
            if ("POST".equals(method)) {
                PreparedStatement cOrder = connection.prepareStatement("INSERT INTO orders " +
                        "(uuid, username) " +
                        "VALUES (?,?)");
                cOrder.setString(1, key);
                JsonNode node = om.readTree(value);
                String username = node.get("username").textValue();
                cOrder.setString(2, username);
                cOrder.execute();
                context.forward(key, value);
            } else if ("PATCH".equals(method)) {
                JsonNode node = om.readTree(value);
                Integer orderAdd = node.get("orderAdd").intValue();
                Integer orderAddRT = node.get("orderAddRT").intValue();
                PreparedStatement cOrder = connection.prepareStatement("UPDATE orders " +
                        "SET o" + orderAdd + " = ?," +
                        " or" + orderAdd + " = ?" +
                        "where uuid = ?");
                cOrder.setInt(1, orderAdd);
                cOrder.setInt(2, orderAddRT);
                cOrder.setString(3, key);
                cOrder.execute();
                context.forward(key, value);
            } else {

            }
        } catch (SQLException e) {
            log.error("Can't store message", e);
        } catch (JsonMappingException e) {
            log.error("Can't read data in message", e);
        } catch (JsonProcessingException e) {
            log.error("Can't read data in message", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    log.warn("Can't close connection", e);
                }
            }
        }
    }

    @Override
    public void close() {

    }
}
