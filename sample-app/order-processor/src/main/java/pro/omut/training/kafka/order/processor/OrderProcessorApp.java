package pro.omut.training.kafka.order.processor;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import pro.omut.training.kafka.app.util.AppBootstrap;
import pro.omut.training.kafka.app.util.AppLifecycle;
import pro.omut.training.kafka.app.util.config.EnvConfig;

import javax.sql.DataSource;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
public class OrderProcessorApp implements AppLifecycle {

    private KafkaStreams streams;
    private DataSource ds;

    public static void main(String[] args) {
        new AppBootstrap(new OrderProcessorApp()).start();
    }

    @Override
    public void start() {
        EnvConfig config = new EnvConfig();
        String kafkaHosts = config.get("KAFKA_HOSTS", "localhost:9092");

        String pgUrl = config.get("PG_URL", "jdbc:postgresql://localhost:35432/order_demo");
        String pgUser = config.get("PG_USER", "postgres");
        String pgPassword = config.get("PG_PASSWORD", "example");

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(pgUrl);
        hikariConfig.setUsername(pgUser);
        hikariConfig.setPassword(pgPassword);
        hikariConfig.setMinimumIdle(1);
        hikariConfig.setMaximumPoolSize(5);

        ds = new HikariDataSource(hikariConfig);

        Topology topology = new Topology();
        topology.addSource("ORDER_CMD", "order-command");
        topology.addProcessor("ORDER_PROC", () -> new OrderProcessor(ds), "ORDER_CMD");
        topology.addSink("ORDER_EVENT", "order-event", "ORDER_PROC");

        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "order-processor");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaHosts);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        streams = new KafkaStreams(topology, props);
        streams.setStateListener((newState, oldState) -> {
            if (KafkaStreams.State.ERROR.equals(newState)) {
                System.exit(1);
            }
        });
        streams.setUncaughtExceptionHandler((t, e) -> {
            System.exit(1);
        });
        streams.start();
    }

    @Override
    public void stop() {

    }
}
